package com.udemy.sfg.s07httpbasicauth;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class S07HttpBasicAuthApplication {

    public static void main(String[] args) {
        SpringApplication.run(S07HttpBasicAuthApplication.class, args);
    }

}
