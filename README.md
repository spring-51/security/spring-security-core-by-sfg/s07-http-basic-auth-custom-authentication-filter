# Http Basic Auth Custom Authentication Filter

## References
```ref
1. https://docs.spring.io/spring-security/site/docs/current/reference/html5
```

## L41 - Custom Authentication Filter Overview
```
1. Spring uses BasicAuthenticationFilter for http basic authentication.
2. refer ./L41-CustomAuthFilter.pdf
```

## L43 - Custom Authentication Filter Step - Part 1
```
Step1 - Create a java class as RestHeaderAuthenticationFilter.java (name could be anything).
Step2 - RestHeaderAuthenticationFilter extends AbstractAuthenticationProcessingFilter
  - AbstractAuthenticationProcessingFilter type filter nean is responsible for Authentication
Step3 - create constructor which initialise RequestMatcher
i.e RestHeaderAuthenticationFilter(RequestMatcher requiresAuthenticationRequestMatcher)

Step4 - override attemptAuthentication(HttpServletRequest, HttpServletResponse) method
      - get username and password from header keys
      - Create UsernamePasswordAuthenticationToken object using username and password we get above
        -- UsernamePasswordAuthenticationToken is a token wich works with Spring AuthenticationManager
      - pass the token object to authentication manager

refer - com.udemy.sfg.s07httpbasicauth.security.filters.RestHeaderAuthenticationFilter -> attemptAuthentication(HttpServletRequest, HttpServletResponse)
```

## L44 - Custom Authentication Filter Step - Part 2 - Spring Security Configuration
```
Step5 -  Add RestHeaderAuthenticationFilter to Spring context.
  -- create Filter Object in SecurityConfig class
  -- refer - com.udemy.sfg.s07httpbasicauth.configs.SecurityConfig -> restHeaderAuthenticationFilter()
Step6 - add filter to Spring Security
  - configure RestHeaderAuthenticationFilter such that it will be called before UsernamePasswordAuthenticationFilter
  i.e. RestHeaderAuthenticationFilter plcaed before UsernamePasswordAuthenticationFilter in spring filter chain.
    -- UsernamePasswordAuthenticationFilter - ?
  - http.addFilterBefore(
                restHeaderAuthenticationFilter(authenticationManager()),
                UsernamePasswordAuthenticationFilter.class);
  
  - refer - com.udemy.sfg.s07httpbasicauth.configs.SecurityConfig -> configure(HttpSecurity)
```

## L45 - Debugging Spring Security - Step - Part 3 - Spring Security Configuration
```
Step7 - TO debug spring security, we can check logs
  -- enable logs by adding log property in application properties file
    logging.level.org.springframework.security=debug
    logging.level.com.udemy.sfg.* =debug

Step8 - check logs chhater in console.
Step9 - disable csrf
  http.csrf().disable()
**NOTE** - Afer these steps we will get Http status as 302 instead of 200,
           because default behaviour of doFilter(ServletRequest, ServletResponse, FilterChain) of AbstractAuthenticationProcessingFilter.
           To overcome this we need to override doFilter(ServletRequest, ServletResponse, FilterChain) in RestHeaderAuthenticationFilter
```

## L46 - Custom doFilter - Step - Part 4 - Spring Security Configuration
```
Step10 - override doFilter() and successfulAuthentication() method
- refer - com.udemy.sfg.s07httpbasicauth.security.filters.RestHeaderAuthenticationFilter -> doFilter(ServletRequest, ServletResponse, FilterChain)  
- refer - com.udemy.sfg.s07httpbasicauth.security.filters.RestHeaderAuthenticationFilter -> attemptAuthentication(HttpServletRequest, HttpServletResponse)
- refer - com.udemy.sfg.s07httpbasicauth.security.filters.RestHeaderAuthenticationFilter -> successfulAuthentication(HttpServletRequest, HttpServletResponse, FilterChain, Authentication)

All the steps complted.
Now our custom filter i.e. RestHeaderAuthenticationFilter is completed and it will be used when we pass
 - username as header key - "Api-Key"
 - Password as keader key - "Api-Secret"
 
JUNIT tests
- refer - com.udemy.sfg.s07httpbasicauth.controller.CustomFilterControllerTests -> getApiTest()

**********************************************************
NOTE-
From JUNIT we are getting 200.
From POSTMAN also we are getting 200 BUt in bothe case we are not able to hit Controller.
We are just getting empty response with Http status 200. 
**********************************************************
```

## L47 - Custom Filter Authentication failure 
```
1. When we do not pass custom headers we can pass username and password as basic auth header or in url 
then also our API will be accessed.

2. if we pass custom header but incorrect credential then we will be getting 500, but expection is 401 or 403.
3. We are getting 500 beacuse attemptAuthentication(HttpServletRequest, HttpServletResponse) is throwing 500.
In order to handle this 500 we surrond it with try- catch 
  - refer com.udemy.sfg.s07httpbasicauth.security.filters.RestHeaderAuthenticationFilter -> doFilter(ServletRequest, ServletResponse, FilterChain)
  
4. on checking logs go test  CustomFilterControllerTests -> getApiUsingBadCredentialTest()
 we found that SimpleUrlAuthenticationFailureHandler is responsible for handle unsuccessful authentication.

5. overriding unsuccessfulAuthentication() 

We cn customize login failure by overriding unsuccessfulAuthentication() of  AbstractAuthenticationProcessingFilter 
in RestHeaderAuthenticationFilter.

- refer - com.udemy.sfg.s07httpbasicauth.security.filters.RestHeaderAuthenticationFilter ->unsuccessfulAuthentication(HttpServletRequest, HttpServletResponse, AuthenticationException)
```

## Assignment - Create another Custom Filter reading values from Url query param
```
- refer - com.udemy.sfg.s07httpbasicauth.security.filters.AbstractRestAuthenticationFilter
  -- this base class is created for this assignment since we create new filter
- refer - com.udemy.sfg.s07httpbasicauth.security.filters.RestUrlParamAuthenticationFilter
- refer - com.udemy.sfg.s07httpbasicauth.security.filters.RestHeaderAuthenticationFilter
- refer -  com.udemy.sfg.s07httpbasicauth.configs.SecurityConfig -> restUrlParamAuthenticationFilter()
- refer -  com.udemy.sfg.s07httpbasicauth.configs.SecurityConfig -> configure(HttpSecurity)

JUNIT Tests
- refer - CustomFilterControllerTests -> getApiUsingCorrectCredentialAsQueryParamTest()
- refer - CustomFilterControllerTests -> getApiUsingBadCredentialAsQueryParamTest() 
```